package az.atacademy.restexamples.demo.controller;


import az.atacademy.restexamples.demo.exceptions.BookNotFoundExceptions;
import az.atacademy.restexamples.demo.model.dto.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApiControllerAdvicer {

    @ExceptionHandler(BookNotFoundExceptions.class)
    @ResponseStatus(HttpStatus.OK)
    public BaseResponse bookNotFound(BookNotFoundExceptions ex){

      return  new BaseResponse(ex.getMessage(),99, null);
    }


}
