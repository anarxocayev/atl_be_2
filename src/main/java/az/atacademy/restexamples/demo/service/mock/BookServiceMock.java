package az.atacademy.restexamples.demo.service.mock;

import az.atacademy.restexamples.demo.exceptions.BookNotFoundExceptions;
import az.atacademy.restexamples.demo.model.Book;
import az.atacademy.restexamples.demo.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@Profile("integration")
public class BookServiceMock implements BookService {

    @Value("${db.name}")
    private String dbname;
    @Value("${mail.address}")
    private String mail;
    Logger logger = LoggerFactory.getLogger(BookServiceMock.class);

    public List<Book> getBookList() {
        logger.info("Get book List started");
        logger.info("dbname  is "+dbname);
        logger.info("mail  is "+mail);

        List<Book> list = new ArrayList<>();
        list.add(new Book(1L, "Java SE-integration"));
        list.add(new Book(2L, "SQL-integration"));
        logger.info("books   " + list);
        return list;
    }

    public Book getBookById(long id) {
        return
                getBookList().stream()
                        .filter(b -> b.getId().equals(id))
                        .findFirst()
                        .orElseThrow(() -> new BookNotFoundExceptions(id));

         /*if(book.isPresent()){
            // return new ResponseEntity<>(book.get(), HttpStatus.OK);
         }else{
             return new ResponseEntity<>(new Book(), HttpStatus.NOT_FOUND);
         }*/
    }


}
