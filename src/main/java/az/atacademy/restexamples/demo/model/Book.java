package az.atacademy.restexamples.demo.model;


public class Book {
  private Long id;
  private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Book(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Book() {
    }
    /* //TDD
    GET HTTP 1.1  baseUrl/v1/books/{bookId}

    response

    {
        "statusCode":"String",   /// SUCCESS,NOT_FOUND, INTERNAL_ERROR
        "statusMessage":"String",  //OK,ERROR
        "data":"  {"id","1","name":"Java SE"}  "

    }
    */


}
