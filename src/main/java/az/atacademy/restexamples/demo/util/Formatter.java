package az.atacademy.restexamples.demo.util;

public interface Formatter {
     String format();
}
