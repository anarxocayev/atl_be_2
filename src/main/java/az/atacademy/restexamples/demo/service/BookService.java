package az.atacademy.restexamples.demo.service;

import az.atacademy.restexamples.demo.exceptions.BookNotFoundExceptions;
import az.atacademy.restexamples.demo.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;



public interface BookService {


     List<Book> getBookList() ;

     Book getBookById(long id) ;

}
