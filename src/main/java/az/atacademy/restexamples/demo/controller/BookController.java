package az.atacademy.restexamples.demo.controller;

import az.atacademy.restexamples.demo.model.Book;
import az.atacademy.restexamples.demo.model.dto.BaseResponse;
import az.atacademy.restexamples.demo.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/books")
public class BookController {
    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }


    /*public ResponseEntity<List<Book>> getBookList(){
        return new ResponseEntity<>(bookService.getBookList(), HttpStatus.BAD_REQUEST);
        //return ResponseEntity.ok(bookService.getBookList()) ;
    }*/
    /*public List<Book> getBookList() {
        return bookService.getBookList();
    }*/
    Logger logger = LoggerFactory.getLogger(BookController.class);

    @GetMapping
    BaseResponse<List<Book>> getBookList() {
        logger.info("Get book List started");

        return new BaseResponse<>("SUCESS",
                HttpStatus.OK.value(),
                bookService.getBookList());
    }

    @GetMapping("/{bookId}")
    BaseResponse<Book> getBook(@PathVariable Long bookId)  {
        return new BaseResponse<>("SUCESS",
                HttpStatus.OK.value(),
                bookService.getBookById(bookId));
    }

    /*public ResponseEntity<Book> getBook(@PathVariable Long bookId) {
        return bookService.getBookById(bookId);

    }*/
}
