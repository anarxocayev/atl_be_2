package az.atacademy.restexamples.demo.exceptions;

public class BookNotFoundExceptions extends RuntimeException {

    public BookNotFoundExceptions(Long bookId) {
        super("Book not found with "+bookId+" id");
    }
}
